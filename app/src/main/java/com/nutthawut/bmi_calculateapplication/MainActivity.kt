package com.nutthawut.bmi_calculateapplication

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.nutthawut.bmi_calculateapplication.databinding.ActivityMainBinding
import kotlin.math.roundToInt
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener() { calculateBmi() }
        binding.Weight.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view,
            keyCode) }
        binding.High.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view,
            keyCode) }
    }
    private fun calculateBmi() {
        val WeightString = binding.Weight.text.toString()
        val HighString = binding.High.text.toString()
        if(HighString.trim().length<=0||WeightString.trim().length<=0){
            Toast.makeText(this@MainActivity, "Please input data!", Toast.LENGTH_SHORT).show()
            return
        }
        else {
            val weight = WeightString.toDouble()
            var high = HighString.toDouble()
            high /= 100
            val bmi = weight / (high * high)
            val formattedBMI = (bmi * 100.0).roundToInt() / 100.0
            var proportion =
                when {
                    bmi < 18.5 -> "UnderWeight [ ผอม ] \nภาวะเสี่ยงโรค: มากกว่าคนปกติ"
                    bmi in 18.5..22.9 -> "Normal [ สมส่วน ] \nภาวะเสี่ยงโรค: เท่าคนปกติ"
                    bmi in 23.0..24.9 -> "OverWeight [ น้ำหนักเกิน ] \nภาวะเสี่ยงโรค: อันตรายระดับ 1"
                    bmi in 25.0..29.9 -> "Obese [ สภาวะอ้วน ] \nภาวะเสี่ยงโรค: อันตรายระดับ 2"
                     else -> "Extremly Obese [ สภาวะอ้วนมาก ] \nภาวะเสี่ยงโรค: อันตรายระดับ 3"
                }
            var image =
                when {
                    bmi < 18.5 -> R.drawable.underweight
                    bmi in 18.5..22.9 -> R.drawable.normal
                    bmi in 23.0..24.9 -> R.drawable.overweight
                    bmi in 25.0..29.9 -> R.drawable.obese
                    else -> R.drawable.extreme_obese
                }
            binding.bmiResult.text = "ค่า BMI: ${formattedBMI}"
            binding.proportion.text = "อยู่ในเกณฑ์: ${proportion}"
            binding.imageView.setImageResource(image)
        }
    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}